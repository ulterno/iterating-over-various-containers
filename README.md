# Iterating over various containers

Simple code to check iteration time over multiple containers using various methods.

Made using [this answer by B. Decoster][def]

On Ryzen 5 5600X with 16GB DDR4 3000

```
Reference vector :       43  ms
Reference list :         30  ms
QMap::values() :         1616  ms
QHash::values() :        1333  ms
QMap Java style iterator :       1243  ms
QHash Java style iterator :      1134  ms
QMap  STL style iterator :       1438  ms
QHash STL style iterator :       1135  ms
QMap STL style iterator v2 :     1494  ms
QHash STL style iterator v2 :    1145  ms
```

[def]: https://stackoverflow.com/a/31226494