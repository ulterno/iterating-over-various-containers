#include <QCoreApplication>
#include <QDateTime>
#include <QMap>
#include <QHash>
#include <QVector>
#include <QList>
#include <QDebug>
#include <QRandomGenerator>

void testContainers(int size, int nbIterations){
    QMap<int, int> map;
    QHash<int, int> hash;
    QVector<int> vec;
    QList<int> list;

    volatile int sum = 0;

    for(int i = 0; i<size; ++i){
        int randomInt = qrand()%128;
        map[i] = randomInt;
        hash[i] = randomInt;
        vec.append(randomInt);
        list.append(randomInt);
    }


    // Rererence vector/list
    qint64 start = QDateTime::currentMSecsSinceEpoch();
    for(int i = 0; i<nbIterations; ++i){
        sum = 0;
        for(int j : vec){
            sum += j;
        }
    }
    qint64 end = QDateTime::currentMSecsSinceEpoch();
    qDebug() << "Reference vector : \t" << (end-start) << " ms";

    qint64 startList = QDateTime::currentMSecsSinceEpoch();
    for(int i = 0; i<nbIterations; ++i){
        sum = 0;
        for(int j : list){
            sum += j;
        }
    }
    qint64 endList = QDateTime::currentMSecsSinceEpoch();
    qDebug() << "Reference list : \t" << (endList-startList) << " ms";

    // QMap::values()
    qint64 start0 = QDateTime::currentMSecsSinceEpoch();
    for(int i = 0; i<nbIterations; ++i){
        sum = 0;
        QList<int> values = map.values();
        for(int k : values){
            sum += k;
        }
    }
    qint64 end0 = QDateTime::currentMSecsSinceEpoch();
    qDebug() << "QMap::values() : \t" << (end0-start0) << " ms";

    // QHash::values()
    qint64 startH0 = QDateTime::currentMSecsSinceEpoch();
    for (int iter = 0; iter < nbIterations; ++iter)
    {
        sum = 0;
        QList<int> values = hash.values();
        for(int k : values){
            sum += k;
        }
    }
    qint64 endH0 = QDateTime::currentMSecsSinceEpoch();
    qDebug() << "QHash::values() : \t" << (endH0-startH0) << " ms";


    // Java style iterator
    qint64 start1 = QDateTime::currentMSecsSinceEpoch();
    for(int i = 0; i<nbIterations; ++i){
        sum = 0;
        QMapIterator<int, int> it(map);
        while (it.hasNext()) {
            it.next();
            sum += it.value();
        }
    }
    qint64 end1 = QDateTime::currentMSecsSinceEpoch();
    qDebug() << "QMap Java style iterator : \t" << (end1-start1) << " ms";

    // QHash Java style iterator
    qint64 startH1 = QDateTime::currentMSecsSinceEpoch();
    for(int i = 0; i<nbIterations; ++i){
        sum = 0;
        QHashIterator<int, int> it(hash);
        while (it.hasNext()) {
            it.next();
            sum += it.value();
        }
    }
    qint64 endH1 = QDateTime::currentMSecsSinceEpoch();
    qDebug() << "QHash Java style iterator : \t" << (endH1-startH1) << " ms";


    // STL style iterator
    qint64 start2 = QDateTime::currentMSecsSinceEpoch();
    for(int i = 0; i<nbIterations; ++i){
        sum = 0;
        QMap<int, int>::const_iterator it = map.constBegin();
        auto end = map.constEnd();
        while (it != end) {
            sum += it.value();
            ++it;
        }
    }
    qint64 end2 = QDateTime::currentMSecsSinceEpoch();
    qDebug() << "QMap  STL style iterator : \t" << (end2-start2) << " ms";

    // QHash STL style iterator
    qint64 startH2 = QDateTime::currentMSecsSinceEpoch();
    for(int i = 0; i<nbIterations; ++i){
        sum = 0;
        QHash<int, int>::const_iterator it = hash.constBegin();
        auto end = hash.constEnd();
        while (it != end) {
            sum += it.value();
            ++it;
        }
    }
    qint64 endH2 = QDateTime::currentMSecsSinceEpoch();
    qDebug() << "QHash STL style iterator : \t" << (endH2-startH2) << " ms";


    qint64 start3 = QDateTime::currentMSecsSinceEpoch();
    for(int i = 0; i<nbIterations; ++i){
        sum = 0;
        auto end = map.cend();
        for (auto it = map.cbegin(); it != end; ++it)
        {
            sum += it.value();
        }
    }
    qint64 end3 = QDateTime::currentMSecsSinceEpoch();

    qDebug() << "QMap STL style iterator v2 : \t" << (end3-start3) << " ms";

    qint64 startH3 = QDateTime::currentMSecsSinceEpoch();
    for(int i = 0; i<nbIterations; ++i){
        sum = 0;
        auto end = hash.cend();
        for (auto it = hash.cbegin(); it != end; ++it)
        {
            sum += it.value();
        }
    }
    qint64 endH3 = QDateTime::currentMSecsSinceEpoch();

    qDebug() << "QHash STL style iterator v2 : \t" << (endH3-startH3) << " ms";

    exit(0);
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    if (argc == 3)
    {
        bool okb;
        int size = QString::fromLatin1(argv[1]).toInt(&okb);
        int nbIterations = QString::fromLatin1(argv[2]).toInt(&okb);
        if(okb)
        {
            testContainers(size, nbIterations);
        }
        else
        {
            qDebug () << "Problem in arguments";
            exit (2);
        }
    }
    else
    {
        qDebug () << "Arguments: 1 - size of container; 2 - number of iterations";
        exit (1);
    }

    return a.exec();
}
